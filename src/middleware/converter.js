const applescript = require('applescript');
const util = require("util");
const replaceAll = (str, find, replace) => {
    return str.replace(new RegExp(find, 'g'), replace);
}

const converto = async (req, res) => {
    let filePath = req.file.path;
    let processedPath = replaceAll(filePath, '/', ':');
    console.log(processedPath);

    // part 1
    const open_script = `
    tell application "Adobe InDesign 2022" to activate
    tell application "Adobe InDesign 2022"
        set myDocument to open "Macintosh HD${processedPath}"
    end tell
    tell application "Adobe InDesign 2022" to activate
    tell application "System Events" to tell process "Adobe InDesign 2022"
        key code 53
    end tell
    tell application "Adobe InDesign 2022" to activate
    tell application "Adobe InDesign 2022"
        tell active document
            export format PDF type to "Macintosh HD:Users:sohum:dev:sample_file_transfer:resources:static:assets:uploads:${req.file.originalname}.pdf"
        end tell
    end tell
    tell application "Adobe InDesign 2022" to activate
    tell application "Adobe InDesign 2022"
        close (front document) saving no
    end tell
    `
    let runConversionScript = util.promisify(applescript.execString)

    try {
        await runConversionScript(open_script)
    } catch(err){
        console.log('ERROR occurred')
    }
}

module.exports = converto;