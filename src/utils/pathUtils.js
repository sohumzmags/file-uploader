
const getDestinationPath = () => {
    destinationPath = __dirname.split('/')
    destinationPath.pop()
    destinationPath.pop()
    destinationPath = destinationPath.join('/')
    destinationPath = destinationPath + '/resources/static/assets/uploads/'
    return destinationPath
}

module.exports = getDestinationPath;